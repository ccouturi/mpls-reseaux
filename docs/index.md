# Les secrets des réseaux informatiques

Bienvenue sur le site de la formation *Les secrets des réseaux informatiques*.
Vous trouverez sur ce site les différentes resources et activités qui
seront utilisées au cours de la formation. 

Ces contenus sont également disponibles sur le site de [partage
documentaire de la MPLS](http://devpro.fondation-lamap.org/){target=_blank}. 


Sauf mention contraire, le contenu de ce site est distribué sous licence
Creative Commons [CC
BY](https://creativecommons.org/licenses/?lang=fr){target=_blank}. Vous
pouvez donc réutiliser et modifier ces contenus à l'unique condition
d'en citer les auteurs originaux. N'hésitez pas à vous rapprocher de
nous pour avoir accès aux fichiers source de ces contenus.


Les auteurs:

* Anne-Hélène TUAL - Ingénieure pédagogique à la Maison Pour La Science de Bretagne
* Anthony PENHARD - Enseignant de Technologie au collège de Bréal-sous-Montfort 
* Christophe COUTURIER - Enseignant Chercheur à IMT Atlantique

<!-- Some Markdown text with <span style="color:blue">some *blue* text</span>. -->
<!-- <p class="text-primary">Nullam id dolor id nibh ultricies vehicula ut id elit.</p> -->
<!-- <p class="text-secondary">Nullam id dolor id nibh ultricies vehicula ut id elit.</p> -->

## Planning de la formation

* Première journée **(3h)**
    * Présentation participants **(20')**
    * Présentation de la MPLS, des intervenants et des participants **(10')**
    * Activité collective *"Les réseaux c'est"* **(45')**
    * Pause **(10')**
    * Éclairage scientifique sur les réseaux: Architecture (LAN/WAN) / Modèle en couches / Adresses / Ports **(40')**
    * Activité *"À la découverte de la configuration réseau"* **(45')**

</br>

* Deuxième journée **(3h)**
    * Introduction et bilan session 1 **(15')**
    * Apports théoriques sur les délais dans les réseaux  **(15')**
    * Activité *"Mesure des délais sur Internet"* **(50')**
    * Pause **(10')**
    * Démo *"Capture de trafic avec Wireshark* **(10')**
    * Présentation *"Les enjeux des réseaux aujourd'hui"* **(30')**
    * Échanges de pratiques **(30')**
    * Conclusion **(10')**