# Resources complémentaires

### Présentations utilisées pendant la formation

* Support Genially *["Éclairage scientifique sur les
  réseaux"](https://view.genial.ly/60524fc89b3f0f0d928b5f9c){target=_blank}* (1er
  jour)
* Support Génially *["Retour sur l'activité sur les adresses"](https://view.genial.ly/608725ea101f0b0d197f3dbb){target=_blank}*
* Support des présentations sur*["Les délais et Wireshark"](resources/20210428--MPLS-Delais_et_Wireshark.pdf){target=_blank}*
  (2eme jour)
* Support *["Les enjeux des
  réseaux"](resources/20210428--MPLS-Enjeux_des_reseaux.pdf){target=_blank}*
  (2eme jour)

### Outils en ligne, logiciels et librairies 
* Outils en ligne pour l'analyse de l'internet
    * Un site (parmi d'autres) de ["looking glasses"](https://www.bgp4.as/looking-glasses){target=_blank}
    * Un site (parmi d'autres) pour [trouver votre @IP publique](https://ip.lafibre.info/){target=_blank}
* Des "Weathermaps" permettant de suivre l'état d'un réseau en temps
  réel
    * La ["Weathermap" du réseau
      RENATER](https://www.renater.fr/sites/default/files/weathermap/weathermap_metropole.html){target=_blank}
    * La
      [topologie](https://www.geant.org/resources/publishingimages/geant_topology_map_december_2018.jpg){target=_blank}
      et la
      ["Weathermap"](https://tools.geant.org/portal/links/p-cacti/plugins/weathermap/weathermap-cacti-plugin.php?group_id=2){target=_blank}
      du réseau GÉANT
* Simulateurs de réseau:
    * [Filius](https://ent2d.ac-bordeaux.fr/disciplines/sti-college/2019/09/25/filius-un-logiciel-de-simulation-de-reseau-simple-et-accessible/){target=_blank}:
      un simulateur de réseaux simple permettant de dessiner des
      topologies réseau et de tester des configurations simples (@IP,
      Routage, DNS, Serveur Web...) [lien de téléchargement (en
      allemand)](https://www.lernsoftware-filius.de/Herunterladen){target=_blank}
    * [GNS3](https://www.gns3.com/){target=_blank}: Émulateur de réseau un peu plus
      complexe (mais puissant) que Filius. GNS3 permet notamment
      d'intégrer de machines virtuelles et des images de routeur (à
      condition d'en avoir!) dans les émulations.
* Quelques logiciels et librairies
    * [Wireshark](https://www.wireshark.org/){target=_blank}: un outil
      de capture de trafic
    * [NMap](https://nmap.org/){target=_blank}: Pour les utilisateurs
      avancés: un outil de scan de réseau 
    * [hrPing](https://www.cfos.de/en/ping/ping.htm){target=_blank}: une
      commande `ping` plus évoluée pour windows offrant notamment un
      horodatage plus précis
    * [Scapy](https://scapy.net/){target=_blank}: Pour les utilisateurs
      avancés: à la fois une librairie (Python) et un outil de test
      réseau
    * [Flask](https://palletsprojects.com/p/flask/){target=_blank}:
      Librairie Python pour développer facilement des serveurs et des
      API web.
    * [FastAPI](https://fastapi.tiangolo.com/){target=_blank}: Une autre
      librairie Python (ou plutôt un framework) pour développer
      facilement des API web.


### Les liens issus de nos échanges:

  * Sous Android, pour vérifier les pisteurs installés sur vos applis
    vous pouvez utiliser [Exodus Privacy](https://exodus-privacy.eu.org/fr/){target=_blank}
  * Livre: [Mindfuck de Christophe Wylie](https://www.babelio.com/livres/Wylie-Mindfuck/1321716){target=_blank}, le lanceur d'alerte de Cambridge Analytica
  * BD: [La Revue Dessinée](https://www.4revues.fr/la-revue-dessinee/),
    avec un numéro sur Cambridge Analytica (n°25) 
  * Documentaire: [Nothing to Hide](https://fr.wikipedia.org/wiki/Nothing_to_Hide),
      s'intéresse aux effets de la surveillance de
    masse sur les individus et la société 
  * Livre: [Comédies françaises d'Eric Reinhardt](https://www.babelio.com/livres/Reinhardt-Comedies-francaises/1239543){target=_blank}
  * Sur la surveillance numérique, lire le travail de [Shoshana
    Zuboff](https://fr.wikipedia.org/wiki/Shoshana_Zuboff)
  * Le travail d'une classe de 4eme qui a créé elle même une [vidéo de
    Fake
    News](https://blog.francetvinfo.fr/l-instit-humeurs/2021/02/20/video-des-eleves-denoncent-les-chats-pour-lutter-contre-le-complotisme.html) <!-- lien direct vers la video: https://vimeo.com/166931978 -->
  * Description de l'activité proposée par Séverin et ses collègues 
    de l'académie de Grenoble à télécharger [ici](https://filesender.renater.fr/?s=download&token=73fefedc-2e2c-4597-b5e7-9347af9f052c)

### MOOC et Livres

* Le [MOOC Principes des réseaux de
  données](https://www.fun-mooc.fr/fr/cours/principes-des-reseaux-de-donnees/?edit&language=fr){target=_blank}
  réalisé par l'IMT
* Le [livre correspondant au MOOC Principe des réseaux de
  données](https://www.eyrolles.com/Informatique/Livre/les-reseaux-9782746246591/),
  édité par les auteurs du MOOC. Mais il semble en rupture chez
  l'éditeur :-(
* Un [MOOC dédié à IPv6](https://www.fun-mooc.fr/fr/cours/objectif-ipv6/), par l'IMT
* Le livre ["Réseaux" de Andrew
  Tanenbaum](https://www.eyrolles.com/Informatique/Livre/reseaux-9782744075216/)
  qui est une référence dans les universités du monde entier
* Le livre ["Les Réseaux" de Guy
  Pujolle](https://www.eyrolles.com/Informatique/Livre/les-reseaux-9782212675351/),
  best seller en France

